# Arlity API SDK PHP

SDK w PHP do integracji z API systemu Arlity.

SDK jest przystosowane pod użycie z Composer.
Alternatywnie można pobrać źródło i załadować do projektu plik `/src/autoload.php`, który odpowiada za automatyczne ładowanie plików.

## Wymagania

- PHP 5.6 lub nowsze
- Rozszerzenia do PHP:
  - json
  - curl

## Konfiguracja

1. Konfigurację zaczynamy od pliku `/src/Config.php` (najlepiej jest nadpisać właściwości przed utworzeniem obiektu SDK, zamiast edytować plik).
2. Tworzymy nowy obiekt klasy `new \ArlitySDK\SDK;`.
3. Po utworzeniu obiektu automatycznie wykona się proces autoryzacji, z danymi ustawionymi w klasie `ArlitySDK\Config`, wiec obiekt powinien być tworzony, tylko jeśli chcemy go rzeczywiście użyć.
4. Jeśli dane w configu są poprawne, powinniśmy móc już używać obiektu do zapytań API. 

Aby użyć API, operujemy na obiekcie utworzonym z klasy `\ArlitySDK\SDK`. Ma ona publiczne parametry, odpowiadające zasobom api (np. `auth`, `category`, `product`), metody tych właściwości odpowiadają konkretnym zasobom. Przykłady użyciu są w katalogu `/examples`.

Większość metod do dodawania i edycji zasobów wymaga obiektów klasy z przestrzeni nazwa `ArlitySDK\Lib\Entity`. Podobnie przy pobieraniu obiektów zwracane są takie same obiekty. 

W przypadku błędów z API, w odpowiedzi powinniśmy otrzymać obiekt klasy `ArlitySDK\Lib\ResponseError`, w przypadku wyjątków natomiast `ArlitySDK\Lib\SDKException`.

## System autoryzacji

Jeśli używamy tego SDK, nie musimy się nim przejmować, autoryzacja przebiegnie automatycznie.

Więcej informacji o autoryzacji można znaleźć tutaj: https://gitlab.com/arlity/api-doc/-/wikis/Resources/Auth 

## Logi

Jeśli w konfiguracji włączymy `ArlitySDK\Config::$log_curl_queries`, wtedy będą logowane wszystkie zapytanie do API, w tym te związane z autoryzacją, każde w osobnym pliku.
  
Pliki będą zapisywane w lokalizacji podanej w `ArlitySDK\Config::$log_path`. Wewnątrz zostaną utworzone katalogi dla roku, miesiąca i dnia. W rezultacie pliki będą zapisywane pod taką ścieżką: `YYYY/mm/dd/HH:ii:ss:uu_MATHOD_url`.
  
W samym pliku logu będzie 5 sekcji, oddzielonych pustymi liniami:
- adres url
- zrzut obiektu curl
- przesłane parametry
- nagłówki odpowiedzi
- treść odpowiedzi

------

## API

Api jest oparte na REST, wyniki zwraca i przyjmuje w formacie JSON.

### Dokumentacja

Dokumentacja do samego API znajduje się poniższym projekcie:

https://gitlab.com/arlity/api-doc