<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy daną kategorię
	$category = $SDK -> category -> get( 1 );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $category instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $category );
	}
	else
	{
		echo 'Kategoria:', END_LINE;
		// Tutaj dostaniemy obiekt \ArlitySDK\Lib\Entity\CategoryEntity
		var_dump( $category );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}