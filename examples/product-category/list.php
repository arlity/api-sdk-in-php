<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy kolekcję dostępnych kategorii dla naszego użytkownika
	$categories = $SDK -> category -> getList();

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $categories instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $categories );
	}
	else
	{
		echo 'Lista kategorii:', END_LINE;
		// Tutaj dostaniemy tablicę wypełnionych obiektów \ArlitySDK\Lib\Entity\CategoryEntity
		var_dump( $categories );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}