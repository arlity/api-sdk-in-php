<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy kolekcję dostępnych produktów dla naszego użytkownika
	$products = $SDK -> product -> getList();

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $products instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $products );
	}
	else
	{
		echo 'Lista produktów:', END_LINE;
		// Tutaj dostaniemy tablicę wypełnionych obiektów \ArlitySDK\Lib\Entity\ProductEntity
		var_dump( $products );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}