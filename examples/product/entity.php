<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy dany produkt
	$product = $SDK -> product -> get( 517 );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $product instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $product );
	}
	else
	{
		echo 'Produkt:', END_LINE;
		/**
		 * Tutaj dostaniemy obiekt
		 * @see \ArlitySDK\Lib\Entity\ProductEntity
		 */
		var_dump( $product );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}