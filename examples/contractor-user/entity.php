<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy danego kontrahenta.
	// Wyszukujemy go po kodzie (w momencie dodawania może do niego dopisać się prefix!), więc wpierw powinniśmy odpalić plik add.php.
	$contractor = $SDK -> contractor -> get( 'test_dodawania' );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $contractor instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $contractor );
	}
	else
	{
		echo 'Kontrahent:', END_LINE;
		/**
		 * Tutaj dostaniemy obiekt
		 * @see \ArlitySDK\Lib\Entity\ContractorEntity
		 */
		var_dump( $contractor );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
	$e -> getTraceAsString();
	die;
}