<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

use \ArlitySDK\Lib\Entity\ContractorUserEntity;

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy danego użytkownika.
	// Wyszukujemy go po loginie (w momencie dodawania może do niego dopisać się prefix!), więc wpierw powinniśmy odpalić plik add.php.
	$user = $SDK -> contractor_user -> get( 'test_dodawania' );
	if( !( $user instanceof ContractorUserEntity ))
	{
		echo 'Brak użytkownika o podanym loginie.', END_LINE;
		die;
	}
	$user -> name = 'Test edycji z API';

	// Uaktualniamy dane
	$user = $SDK -> contractor_user -> edit( $user );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $user instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $user );
	}
	else
	{
		echo 'Kontrahent:', END_LINE;
		/**
		 * Tutaj dostaniemy obiekt
		 * @see ContractorUserEntity
		 * Uwaga! Login może się zmienić, jeśli jest ustawiony prefiks!
		 */
		var_dump( $user );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}