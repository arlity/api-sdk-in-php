<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Usuwamy danego użytkownika.
	// Wyszukujemy go po loginie (w momencie dodawania może do niego dopisać się prefix!), więc wpierw powinniśmy odpalić plik add.php.
	$success = $SDK -> contractor_user -> remove( 'test_dodawania' );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $success === true )
	{
		echo 'Usunięto użytkownika.', END_LINE;
	}
	else
	{
		echo 'Error:', END_LINE;
		var_dump( $success );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}