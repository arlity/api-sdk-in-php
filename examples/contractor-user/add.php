<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

use \ArlitySDK\Lib\Entity\ContractorUserEntity;

try
{
	$SDK = new ArlitySDK\SDK();

	$user = new ContractorUserEntity();
	$user -> login = 'test_dodawania';
	$user -> password = 'my secret password';
	$user -> name = 'Użytkownik z API';

	// Dodajemy użytkownika
	$user = $SDK -> contractor_user -> add( $user );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $user instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $user );
	}
	else
	{
		echo 'Użytkownik:', END_LINE;
		/**
		 * Tutaj dostaniemy obiekt
		 * @see ContractorUserEntity
		 */
		var_dump( $user );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}