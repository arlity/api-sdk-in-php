<?php
/**
 * Plik z pomocami do przykładów
 * NIE ŁADUJ GO do kodu produkcyjnego
 */

if( php_sapi_name() == 'cli' )
{
	define( 'END_LINE', "\n" );
}
else
{
	echo '<pre>';
	define( 'END_LINE', '<br>' );
}