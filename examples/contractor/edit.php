<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

use \ArlitySDK\Lib\Entity\ContractorEntity;

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy danego kontrahenta.
	// Wyszukujemy go po kodzie (w momencie dodawania może do niego dopisać się prefix!), więc wpierw powinniśmy odpalić plik add.php.
	$contractor = $SDK -> contractor -> get( 'test_dodawania' );
	if( !( $contractor instanceof ContractorEntity ))
	{
		echo 'Brak kontrahenta o podanym kodzie.', END_LINE;
		die;
	}
	$contractor -> name = 'Test edycji z API';

	// Uaktualniamy dane
	$contractor = $SDK -> contractor -> edit( $contractor );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $contractor instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $contractor );
	}
	else
	{
		echo 'Kontrahent:', END_LINE;
		/**
		 * Tutaj dostaniemy obiekt
		 * @see ContractorEntity
		 * Uwaga! Kod kontrahenta może się zmienić, jeśli jest ustawiony prefiks!
		 */
		var_dump( $contractor );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}