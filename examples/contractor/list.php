<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

try
{
	$SDK = new ArlitySDK\SDK();

	// Pobieramy kolekcję dostępnych kontrahentów dla naszego użytkownika
	$contractors = $SDK -> contractor -> getList();

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $contractors instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $contractors );
	}
	else
	{
		echo 'Lista kontrahentów:', END_LINE;
		/**
		 * Tutaj dostaniemy tablicę wypełnionych obiektów
		 * @see \ArlitySDK\Lib\Entity\ContractorEntity
		 */
		var_dump( $contractors );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}