<?php

require_once '../_utils.php';
require_once '../../src/autoload.php';

use \ArlitySDK\Lib\Entity\ContractorEntity;

try
{
	$SDK = new ArlitySDK\SDK();

	$contractor = new ContractorEntity();
	$contractor -> code = 'test_dodawania';
	$contractor -> type = ContractorEntity::TYPE_B2C;
	$contractor -> name = 'Test dodawania z API';

	// Dodajemy kontrahenta
	$contractor = $SDK -> contractor -> add( $contractor );

	// Sprawdzamy, czy udało uzyskać się prawidłową odpowiedź z serwera
	if( $contractor instanceof ArlitySDK\Lib\ResponseError )
	{
		echo 'Error:', END_LINE;
		var_dump( $contractor );
	}
	else
	{
		echo 'Kontrahent:', END_LINE;
		/**
		 * Tutaj dostaniemy obiekt
		 * @see ContractorEntity
		 * Uwaga! Kod kontrahenta może się zmienić, jeśli jest ustawiony prefiks!
		 */
		var_dump( $contractor );
	}

	echo END_LINE;
}
catch( \Exception $e )
{
	echo $e -> getMessage() , END_LINE ,
		$e -> getFile() . ':' . $e -> getLine() , PHP_EOL,
		$e -> getTraceAsString();
	die;
}