<?php

namespace ArlitySDK\Lib;

/**
 * Klasa do obsługi zapytań curl
 */
class cURL
{
	/**
	 * @var resource
	 */
	private $handle_curl;
	/**
	 * @var array
	 */
	private $curl_options;
	/**
	 * @var array
	 */
	private $request_headers = [];
	/**
	 * @var string
	 */
	private $request_method = 'GET';
	/**
	 * @var string
	 */
	private $request_url = '';
	/**
	 * @var string
	 */
	private $request_prepared_url = '';
	/**
	 * @var array
	 */
	private $request_params = [];
	/**
	 * @var string
	 */
	private $response_header = '';
	/**
	 * @var string
	 */
	private $response_body = '';
	/**
	 * @var array
	 */
	private $curl_info = [];
	/**
	 * @var string
	 */
	private $upload_file_path = null;

	/**
	 * cURL constructor.
	 * @param string $request_method
	 * @see self::setMethod()
	 */
	public function __construct( $request_method = null )
	{
		if( $request_method )
			$this -> setMethod( $request_method );

		$this -> curl_options = [
			CURLOPT_RETURNTRANSFER => true, // zwracanie treści strony
			CURLOPT_FOLLOWLOCATION => true, // przekierowania, przy mniej standardowych zapytaniach mogą być niezbędne
			CURLOPT_CONNECTTIMEOUT => 5, // timeout połączenia
			CURLOPT_MAXREDIRS => 3, // maksymalna liczba przekierowań
			CURLOPT_TIMEOUT => 10, // timeout
			//CURLOPT_FAILONERROR => true, // czy ma się wykładać na plecy, jeśli serwer api zwróci błąd
			CURLOPT_SSL_VERIFYPEER => false, // ignorowanie ssl, chyba mało poprawne politycznie, ale działa i nie trzeba się bawić z certyfikatami
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_HEADER => true, // czy ma zwracać w wyniku header odpowiedzi
			CURLINFO_HEADER_OUT => true, // czy wyświetlać header zapytania w info, wywala go do logów
		];
	}

	/**
	 * Wykonanie zapytania
	 */
	public function exec()
	{
		$this -> handle_curl = curl_init();
		$this -> appendParamsToHandle();
		$this -> appendOptionsToHandle();
		$this -> appendHeadersToHandle();
		curl_setopt( $this -> handle_curl, CURLOPT_URL, $this -> request_prepared_url );
		$this -> processResponse( curl_exec( $this -> handle_curl ));
		curl_close( $this -> handle_curl );
	}

	/**
	 * Wykonaj i zapisz wynik do pliku
	 * @param string $file_path_to_save - ścieżka do zapisu
	 * @return bool - czy powodzenie
	 */
	public function executeToFile( $file_path_to_save )
	{
		$file = fopen( $file_path_to_save, 'w+');
		$this -> appendCurlOption( CURLOPT_BINARYTRANSFER, true );
		$this -> appendCurlOption( CURLOPT_FILE, $file );
		$this -> appendCurlOption( CURLOPT_TIMEOUT, 600 );
		$this -> appendCurlOption( CURLOPT_HEADER, false );
		$this -> exec();
		fclose( $file );

		return file_exists( $file_path_to_save ) && filesize( $file_path_to_save ) > 0;
	}

	/**
	 * Dodanie opcji curla, do zapytania
	 */
	private function appendOptionsToHandle()
	{
		if( !empty( $this -> curl_options ))
			curl_setopt_array( $this -> handle_curl, $this -> curl_options );
	}

	/**
	 * Dodanie nagłówków do zapytania
	 */
	private function appendHeadersToHandle()
	{
		if( !empty( $this -> request_headers ))
			curl_setopt( $this -> handle_curl, CURLOPT_HTTPHEADER, $this -> request_headers );
	}

	/**
	 * Dodanie parametrów, do zapytania
	 */
	private function appendParamsToHandle()
	{
		$this -> request_prepared_url = $this -> request_url;

		if( $this -> request_method === 'GET' )
		{
			$this -> request_prepared_url .= ( strpos( $this -> request_url, '?' ) !== false ? '&' : '?' );
			$this -> request_prepared_url .= $this -> generateParamsUrlString();
		}
		else if( $this -> request_method === 'FORM' )
		{
			$this -> appendCurlOption( CURLOPT_POST, 1 );
			$this -> appendCurlOption( CURLOPT_POSTFIELDS, $this -> generateParamsUrlString() );
			$this -> appendRequestHeader( 'Content-Type: application/x-www-form-urlencoded' );
		}
		else if( in_array( $this -> request_method, [ 'POST', 'PUT', 'PATCH', 'DELETE' ]))
		{
			$this -> appendCurlOption( CURLOPT_POST, 1 );
			$this -> appendCurlOption( CURLOPT_POSTFIELDS, json_encode( $this -> request_params ));
			$this -> appendCurlOption( CURLOPT_CUSTOMREQUEST, $this -> request_method );
			$this -> appendRequestHeader( 'Content-Type: application/json' );
		}
		else if( $this -> request_method === 'UPLOAD' )
		{
			if( function_exists( 'curl_file_create' ))
				$file = curl_file_create( realpath( $this -> upload_file_path ));
			else
				$file = '@' . realpath( $this -> upload_file_path );

			$this -> appendCurlOption(CURLOPT_POST, 1 );
			$this -> appendCurlOption(CURLOPT_POSTFIELDS, [ 'file' => $file ] );
			$this -> appendCurlOption(CURLOPT_TIMEOUT, 600 );
		}
	}

	/**
	 * Zamiana tablicy parametrów na string
	 * @return string
	 */
	private function generateParamsUrlString()
	{
		$url_params_string = '';
		foreach( $this -> request_params as $key => $value )
			$url_params_string .= $key.'='.urlencode( $value ).'&';

		return trim( $url_params_string, '&' );
	}

	/**
	 * Przetwarzanie odpowiedzi i podział na nagłówek/body
	 * @param string $_str_response
	 */
	private function processResponse( $_str_response )
	{
		if( isset( $this -> curl_options[CURLOPT_HEADER] ) && $this -> curl_options[CURLOPT_HEADER] == true )
		{
			$header_size = curl_getinfo( $this -> handle_curl, CURLINFO_HEADER_SIZE );
			$this -> response_header = substr( $_str_response, 0, $header_size );
			$this -> response_body = substr( $_str_response, $header_size );
		}
		else if( isset( $this -> curl_options[CURLOPT_RETURNTRANSFER] ) && $this -> curl_options[CURLOPT_RETURNTRANSFER] == true )
			$this -> response_body = $_str_response;

		$this -> curl_info = curl_getinfo( $this -> handle_curl );
	}

	/**
	 * Ustaw url do zapytania
	 * @param string $request_url
	 * @see curl_setopt
	 */
	public function setRequestUrl( $request_url )
	{
		$this -> request_url = $request_url;
	}

	/**
	 * Nadpisanie wszystkich opcji curla, nową tablicą
	 * @param array $curl_options - tablica w formie klucz - wartość
	 * @see curl_setopt
	 */
	public function setCurlOptions( array $curl_options )
	{
		$this -> curl_options = $curl_options;
	}

	/**
	 * Dodanie nowych opcji curl z tablicy
	 * @param array $curl_options - tablica w formie klucz - wartość
	 * @see curl_setopt
	 */
	public function appendCurlOptions( array $curl_options )
	{
		$this -> curl_options = array_merge( $this -> curl_options, $curl_options );
	}

	/**
	 * Dodanie opcji curl
	 * @param int $const_key
	 * @param mixed $value
	 */
	public function appendCurlOption( $const_key, $value )
	{
		$this -> curl_options[$const_key] = $value;
	}

	/**
	 * Ustawienie nagłówków z tablicy
	 * @param array $request_headers - tablica z nagłówkami w wartościach
	 */
	public function setRequestHeaders( array $request_headers )
	{
		$this -> request_headers = $request_headers;
	}

	/**
	 * Dodanie nagłówków z tablicy
	 * @param array $request_headers - tablica z nagłówkami w wartościach
	 */
	public function appendRequestHeaders( array $request_headers )
	{
		$this -> request_headers = array_merge( $this -> request_headers, $request_headers );
	}

	/**
	 * Dodanie nowego nagłówka
	 * @param string $single_header
	 */
	public function appendRequestHeader( $single_header )
	{
		$this -> request_headers[] = $single_header;
	}

	/**
	 * Ustawienie parametrów POST/GET
	 * @param array $request_params - klucz => wartość
	 */
	public function setRequestParams( $request_params )
	{
		$this -> request_params = $request_params;
	}

	/**
	 * Dodanie parametrów POST/GET z tablicy
	 * @param array $request_params
	 */
	public function appendRequestParams( $request_params )
	{
		$this -> request_params = array_merge( $this -> request_params, $request_params );
	}

	/**
	 * Dodanie parametru POST/GET
	 * @param string $key
	 * @param string $value
	 */
	public function appendRequestParam( $key, $value )
	{
		$this -> request_params[$key] = $value;
	}

	/**
	 * Ustawienie metody zapytania
	 * GET - parametry zostaną dodane do linku
	 * POST/PUT/PATCH/DELETE - parametry zostaną dodane do body, jako json
	 * FORM - parametry zostaną wysłane jak formularz html (POST)
	 * @param string $_str_method - GET/POST/PUT/PATCH/DELETE/FORM
	 */
	public function setMethod( $_str_method )
	{
		$this -> request_method = strtoupper( $_str_method );
	}

	/**
	 * @param $file_path
	 */
	public function setUploadFilePath( $file_path )
	{
		$this -> upload_file_path = $file_path;
	}

	/**
	 * @return string
	 */
	public function getRequestUrl()
	{
		return $this -> request_url;
	}

	/**
	 * @return array
	 */
	public function getRequestHeaders()
	{
		return $this -> request_headers;
	}

	/**
	 * @return array
	 */
	public function getRequestParams()
	{
		return $this -> request_params;
	}

	/**
	 * @return string
	 */
	public function getRequestMethod()
	{
		return $this -> request_method;
	}

	/**
	 * Pobranie body odpowiedzi
	 * @return string
	 */
	public function getResponseBody()
	{
		return $this -> response_body;
	}

	/**
	 * Pobranie nagłówków odpowiedzi
	 * @return string
	 */
	public function getResponseHeader()
	{
		return $this -> response_header;
	}

	/**
	 * @return mixed
	 */
	public function getResponseStatusCode()
	{
		return $this -> curl_info['http_code'];
	}

	/**
	 * @return array
	 */
	public function getCurlOptions()
	{
		return $this -> curl_options;
	}

	/**
	 * @return array
	 */
	public function getCurlInfo()
	{
		return $this -> curl_info;
	}
}