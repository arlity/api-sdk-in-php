<?php


namespace ArlitySDK\Lib;


class FileSystem
{
	/**
	 * Metoda do tworzenia katalogu, jeśli nie istnieje.
	 * @param string $_str_path - ścieżka
	 * @param int $_oct_rights - prawa dla katalogu, ustawiane przy tworzeniu, w systemie ósemkowym (0 z przodu), bez apostrofów!
	 * @return boolean - true, jeśli istnieje lub się udało utworzyć, inaczej false
	 * @throws SDKException;
	 */
	static public function createDirIfNoExists( $_str_path, $_oct_rights = 0777 )
	{
		if( is_dir( $_str_path ))
			return true;

		if( !is_dir( dirname( $_str_path )))
			self::createDirIfNoExists( dirname( $_str_path ), $_oct_rights );

		if( mkdir( $_str_path ))
		{
			chmod( $_str_path, $_oct_rights );
			return true;
		}
		else
			throw new SDKException( 'Nie udało utworzyć się katalogu: '.$_str_path );
	}
}
