<?php

namespace ArlitySDK\Lib\Resource;

use ArlitySDK\Lib\Entity\ProductEntity;
use ArlitySDK\Lib\ResponseError;
use ArlitySDK\Lib\SDKException;

class ProductResource extends AbstractResource
{
	/**
	 * Pobranie informacji o produkcie
	 * @param string|int $product_id - ID lub UUID produktu
	 * @return ProductEntity|ResponseError
	 * @throws SDKException
	 */
	public function get( $product_id )
	{
		$result = $this -> request -> GET( '/product/' . $product_id );

		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ProductEntity( $result );
	}

	/**
	 * Pobranie listy produktów
	 * @return ProductEntity[]|ResponseError
	 * @throws \Exception
	 */
	public function getList()
	{
		$result = $this -> request -> GET( '/product' );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
		{
			$collection = [];
			foreach( $result -> _embedded -> product as $data )
				$collection[] = new ProductEntity( $data );
			return $collection;
		}
	}
}