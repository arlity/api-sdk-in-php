<?php

namespace ArlitySDK\Lib\Resource;

use ArlitySDK\Config;
use ArlitySDK\Lib\SDKException;

/**
 * Klasa do obsługi zasobów związanych z autoryzacją, po utworzeniu obiektu sama wykona automatycznie proces autoryzacji
 */
class AuthResource extends AbstractResource
{
	/**
	 * Token do autoryzacji
	 */
	static private $token = false;

	/**
	 * Konstruktor uruchomi dalsze kroki, które będą potrzebne
	 * @throws SDKException
	 */
	public function __construct()
	{
		parent::__construct();

		if( Config::$token_file && is_file( Config::$token_file ))
		{
			$obj_token = json_decode( file_get_contents( Config::$token_file ), true );
			if( isset( $obj_token['token'] ) && isset( $obj_token['expires'] ))
			{
				if( Config::$always_test_token )
					$this -> test( $obj_token['token'] );
				else if( $obj_token['expires'] > time() )
					self::$token = $obj_token['token'];
			}
		}

		if( !self::$token )
		{
			// nie mamy tokena wiec wykonujemy pierwszy krok autoryzacji
			$this -> hello();
		}
	}

	/**
	 * Pierwszy krok autoryzacji, uzyskanie code z api_key
	 * @return bool
	 * @throws SDKException
	 */
	protected function hello()
	{
		$response = $this -> request -> GET(
			'/auth/hello',
			[
				'api_key' => Config::$api_key
			]
		);
		if( isset( $response -> code ))
		{
			// dostaliśmy klucz, wołamy o token
			return $this -> auth( $response -> code );
		}
		else if( isset( $response -> error ))
		{
			// wyskoczył jakiś problem z api
			throw new SDKException( $response -> error );
		}
		else
		{
			// odpowiedź nie rozpoznana, pewnie zły adres albo api umarło
			throw new SDKException( 'Wrong response from API server' );
		}
	}

	/**
	 * Sprawdzenie, czy token jest aktualny, jeśli nie to próba wygenerowania nowego
	 * @param string $_str_token
	 * @return bool
	 * @throws SDKException
	 */
	protected function test( $_str_token )
	{
		$response = $this -> request -> GET(
			'/auth/test',
			[
				'api_key' => Config::$api_key,
				'token' => $_str_token
			]
		);

		if( isset( $response -> code ))
		{
			return $this -> auth( $response -> code );
		}
		else if( isset( $response -> token ))
		{
			self::$token = $response -> token;
			return true;
		}
		else if( isset( $response -> error ))
		{
			throw new SDKException( $response -> error );
		}
		else
		{
			throw new SDKException( 'Wrong response from API server' );
		}
	}

	/**
	 * Uzyskanie tokenu z code
	 * @param string $_str_code - code zwrócone przez api
	 * @return bool
	 * @throws SDKException
	 */
	protected function auth( $_str_code )
	{
		$response = $this -> request -> GET(
			'/auth',
			[
				'code' => $_str_code,
				'auth_code' => md5( $_str_code . Config::$api_secret )
			]
		);
		if( isset( $response -> token ))
		{
			self::$token = $response -> token;
			if( Config::$token_file )
				file_put_contents( Config::$token_file, json_encode( $response ));

			return true;
		}
		else if( isset( $response -> error ))
		{
			throw new SDKException( $response -> error );
		}
		else
		{
			throw new SDKException( 'Wrong response from API server' );
		}
	}

	public static function getToken()
	{
		return self::$token;
	}
}
