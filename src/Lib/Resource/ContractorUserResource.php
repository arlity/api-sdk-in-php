<?php

namespace ArlitySDK\Lib\Resource;

use ArlitySDK\Lib\Entity\ContractorUserEntity;
use ArlitySDK\Lib\ResponseError;
use ArlitySDK\Lib\SDKException;

class ContractorUserResource extends AbstractResource
{
	/**
	 * Pobranie informacji o danym użytkowniku
	 * @param int|string $user_id - ID lub UUID użytkownika
	 * @return ContractorUserEntity|ResponseError
	 * @throws SDKException
	 */
	public function get( $user_id )
	{
		$result = $this -> request -> GET( '/contractor/user/' . $user_id );

		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ContractorUserEntity( $result );
	}

	/**
	 * Pobranie listy użytkowników
	 * @return ContractorUserEntity[]|ResponseError
	 * @throws SDKException
	 */
	public function getList()
	{
		$result = $this -> request -> GET( '/contractor/user' );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
		{
			$collection = [];
			foreach( $result -> _embedded -> contractor_user as $data )
				$collection[] = new ContractorUserEntity( $data );
			return $collection;
		}
	}

	/**
	 * Dodanie nowego użytkownika
	 * @return ContractorUserEntity|ResponseError
	 * @throws SDKException
	 */
	public function add( ContractorUserEntity $user )
	{
		$result = $this -> request -> POST( '/contractor/user', $user );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ContractorUserEntity( $result );
	}

	/**
	 * Edycja użytkownika
	 * @return ContractorUserEntity|ResponseError
	 * @throws SDKException
	 */
	public function edit( ContractorUserEntity $user )
	{
		if( $user -> uuid )
			$id = $user -> uuid;
		else if( $user -> id )
			$id = $user -> id;
		else if( $user -> login )
			$id = $user -> login;
		else
			throw new SDKException('ID, UUID or login param is required to find user.');

		$result = $this -> request -> PUT( '/contractor/user/' . $id, $user );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ContractorUserEntity( $result );
	}

	/**
	 * @param int|string $user_id - ID lub UUID użytkownika
	 * @return ResponseError|bool
	 * @throws SDKException
	 */
	public function remove( $user_id )
	{
		$result = $this -> request -> DELETE( '/contractor/user/' . $user_id );
		if( $result instanceof ResponseError )
			return $result;
		else
			return true;
	}
}
