<?php

namespace ArlitySDK\Lib\Resource;

use ArlitySDK\Lib\Entity\CategoryEntity;
use ArlitySDK\Lib\ResponseError;
use ArlitySDK\Lib\SDKException;

class CategoryResource extends AbstractResource
{
	/**
	 * Pobranie informacji o danej kategorii
	 * @param int|string $category_id - ID lub UUID kategorii
	 * @return CategoryEntity|ResponseError
	 * @throws SDKException
	 */
	public function get( $category_id )
	{
		$result = $this -> request -> GET( '/category/' . $category_id );

		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new CategoryEntity( $result );
	}

	/**
	 * Pobranie listy kategorii
	 * @return CategoryEntity[]|ResponseError
	 * @throws SDKException
	 */
	public function getList()
	{
		$result = $this -> request -> GET( '/category' );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
		{
			$collection = [];
			foreach( $result -> _embedded -> category as $data )
				$collection[] = new CategoryEntity( $data );
			return $collection;
		}
	}
}
