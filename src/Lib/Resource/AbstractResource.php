<?php

namespace ArlitySDK\Lib\Resource;

use ArlitySDK\Lib\Request;

/**
 * Klasa bazowa dla klas obsługujących zasoby
 */
abstract class AbstractResource
{
	/**
	 * @var Request
	 */
	protected $request;

	public function __construct()
	{
		$this -> request = new Request;
	}

	protected function fromArray( array $data )
	{

	}

	protected function toArray()
	{

	}
}
