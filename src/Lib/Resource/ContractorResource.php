<?php

namespace ArlitySDK\Lib\Resource;

use ArlitySDK\Lib\Entity\ContractorEntity;
use ArlitySDK\Lib\ResponseError;
use ArlitySDK\Lib\SDKException;

class ContractorResource extends AbstractResource
{
	/**
	 * Pobranie informacji o danym kontrahencie
	 * @param int|string $contractor_id - ID lub UUID kontrahenta
	 * @return ContractorEntity|ResponseError
	 * @throws SDKException
	 */
	public function get( $contractor_id )
	{
		$result = $this -> request -> GET( '/contractor/' . $contractor_id );

		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ContractorEntity( $result );
	}

	/**
	 * Pobranie listy kontrahentów
	 * @return ContractorEntity[]|ResponseError
	 * @throws SDKException
	 */
	public function getList()
	{
		$result = $this -> request -> GET( '/contractor' );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
		{
			$collection = [];
			foreach( $result -> _embedded -> contractor as $data )
				$collection[] = new ContractorEntity( $data );
			return $collection;
		}
	}

	/**
	 * Dodanie nowego kontrahenta
	 * @return ContractorEntity|ResponseError
	 * @throws SDKException
	 */
	public function add( ContractorEntity $contractor )
	{
		$result = $this -> request -> POST( '/contractor', $contractor );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ContractorEntity( $result );
	}

	/**
	 * Edycja kontrahenta
	 * @return ContractorEntity|ResponseError
	 * @throws SDKException
	 */
	public function edit( ContractorEntity $contractor )
	{
		if( $contractor -> uuid )
			$id = $contractor -> uuid;
		else if( $contractor -> id )
			$id = $contractor -> id;
		else if( $contractor -> code )
			$id = $contractor -> code;
		else
			throw new SDKException('ID, UUID or code param is required to find contractor.');

		$result = $this -> request -> PUT( '/contractor/' . $id, $contractor );
		if( !is_object( $result ) || $result instanceof ResponseError )
			return $result;
		else
			return new ContractorEntity( $result );
	}

	/**
	 * @param int|string $contractor_id - ID lub UUID kontrahenta
	 * @return ResponseError|bool
	 * @throws SDKException
	 */
	public function remove( $contractor_id )
	{
		$result = $this -> request -> DELETE( '/contractor/' . $contractor_id );
		if( $result instanceof ResponseError )
			return $result;
		else
			return true;
	}
}
