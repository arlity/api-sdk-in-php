<?php

namespace ArlitySDK\Lib;

use ArlitySDK\Config;
use ArlitySDK\Lib\Resource\AuthResource;
use ArlitySDK\Lib\Entity\AbstractEntity;
use stdClass;

class Request
{
	/**
	 * Zapytanie do api
	 * @param string $url - pełen url do zasobu lub z pominięciem adresu ustawionym w configu jako api_url
	 * @param array|AbstractEntity $request_params - tablica z parametrami, w formie klucz - wartość
	 * @param string $method - typ zapytania, get, post put itd. poza get, parametry zostaną wysłane jako json, bo
	 *     tak żąda api
	 * @param array $request_headers - tablica z ewentualnymi dodatkowymi nagłówkami
	 * @return string|stdClass|ResponseError
	 * @throws SDKException
	 */
	public function callAPI( $url, $request_params = [], $method = 'get', array $request_headers = [] )
	{
		$url = $this -> prependApiUrl( $url );
		$url = $this -> appendTokenToUrl( $url );

		$curl = new cURL( $method );
		$curl -> setRequestUrl( $url );
		$curl -> appendRequestParams( (array) $request_params );
		$curl -> appendRequestHeaders( $request_headers );
		$curl -> exec();

		$this -> logQuery( $curl, $url, $request_params );
		return $this -> getConvertedResponse(
			$curl,
			$curl -> getResponseBody()
		);
	}

	/**
	 * @param string $url
	 * @param array|AbstractEntity $request_params
	 * @param array $request_headers
	 * @return string|stdClass|ResponseError
	 * @throws SDKException
	 */
	public function GET( $url, $request_params = [], array $request_headers = [] )
	{
		return $this -> callAPI( $url, $request_params, 'get', $request_headers );
	}

	/**
	 * @param string $url
	 * @param array|AbstractEntity $request_params
	 * @param array $request_headers
	 * @return string|stdClass|ResponseError
	 * @throws SDKException
	 */
	public function POST( $url, $request_params = [], array $request_headers = [] )
	{
		return $this -> callAPI( $url, $request_params, 'post', $request_headers );
	}

	/**
	 * @param string $url
	 * @param array|AbstractEntity $request_params
	 * @param array $request_headers
	 * @return string|stdClass|ResponseError
	 * @throws SDKException
	 */
	public function PUT( $url, $request_params = [], array $request_headers = [] )
	{
		return $this -> callAPI( $url, $request_params, 'put', $request_headers );
	}

	/**
	 * @param string $url
	 * @param array|AbstractEntity $request_params
	 * @param array $request_headers
	 * @return string|stdClass|ResponseError
	 * @throws SDKException
	 */
	public function PATCH( $url, $request_params = [], array $request_headers = [] )
	{
		return $this -> callAPI( $url, $request_params, 'patch', $request_headers );
	}

	/**
	 * @param string $url
	 * @param array|AbstractEntity $request_params
	 * @param array $request_headers
	 * @return string|stdClass|ResponseError
	 * @throws SDKException
	 */
	public function DELETE( $url, $request_params = [], array $request_headers = [] )
	{
		return $this -> callAPI( $url, $request_params, 'delete', $request_headers );
	}

	/**
	 * @param $url
	 * @param array $request_params
	 * @param string $file_path_to_save
	 * @return string - ścieżka do pliku
	 */
	public function download( $url, array $request_params = [], $file_path_to_save = null )
	{
		set_time_limit( 0 );

		$url = $this -> prependApiUrl( $url );
		$url = $this -> appendTokenToUrl( $url );

		if( !$file_path_to_save )
		{
			$file_path_to_save = rtrim( Config::$download_dir, '/' ) . '/' . uniqid();
		}

		$curl = new cURL();
		$curl -> setRequestUrl( $url );
		$curl -> appendRequestParams( $request_params );
		$curl -> executeToFile( $file_path_to_save );

		return $file_path_to_save;
	}

	/**
	 * @param string $url
	 * @param string $file_path
	 * @param array $request_params
	 * @param array $request_headers
	 * @return ResponseError|stdClass|string
	 * @throws SDKException
	 */
	public function upload( $url, $file_path = null, array $request_params = [], array $request_headers = [] )
	{
		set_time_limit( 0 );

		$url = $this -> prependApiUrl( $url );
		$url = $this -> appendTokenToUrl( $url );

		$curl = new cURL( $file_path ? 'upload' : 'post' );
		$curl -> setRequestUrl( $url );
		if( $file_path )
			$curl -> setUploadFilePath( $file_path );
		$curl -> appendRequestHeader( 'Data: '. json_encode( $request_params ));
		$curl -> appendRequestHeaders( $request_headers );
		$curl -> exec();

		$this -> logQuery( $curl, $url, $request_params );
		return $this -> getConvertedResponse(
			$curl,
			$curl -> getResponseBody()
		);
	}

	/**
	 * @param string $url
	 * @return string
	 */
	private function prependApiUrl( $url )
	{
		if( substr( $url, 0, 4 ) !== 'http' )
		{
			return rtrim( Config::$api_url, '/' ) . '/' . ltrim( $url, '/' );
		}

		return $url;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	private function appendTokenToUrl( $url )
	{
		if( AuthResource::getToken() )
		{
			$url .= (( strpos( $url, '?' ) !== false ) ? '&' : '?' ).'token='.AuthResource::getToken();
		}

		return $url;
	}

	/**
	 * @param cURL $curl
	 * @param string $response
	 * @return string|stdClass|ResponseError
	 */
	private function getConvertedResponse( $curl, $response )
	{
		$obj_response = json_decode( $response );

		return $curl -> getResponseStatusCode() >= 400 ?
			new ResponseError( $obj_response ) :
			$obj_response;
	}

	/**
	 * @param cURL $curl
	 * @param string $url
	 * @param array $params
	 * @throws SDKException
	 */
	private function logQuery( cURL $curl, $url, $params )
	{
		if( !Config::$log_curl_queries )
			return;

		$file_path = $this -> generateLogFilePath( $curl, $url );

		FileSystem::createDirIfNoExists( dirname( $file_path ));

		$content = '--- URL ---'.PHP_EOL.$url.PHP_EOL.PHP_EOL;
		$content .= '--- cURL Data ---'.PHP_EOL.print_r( $curl -> getCurlInfo(), true ).PHP_EOL;
		$content .= '--- Params in JSON ---'.PHP_EOL.json_encode( $params, JSON_PRETTY_PRINT ).PHP_EOL.PHP_EOL;
		$content .= '--- Response headers ---'.PHP_EOL.$curl -> getResponseHeader().PHP_EOL;
		$content .= '--- Response body ---'.PHP_EOL;
		if( ( $object = json_decode( $curl -> getResponseBody() ) ) !== null )
			$content .= json_encode( $object, JSON_PRETTY_PRINT );
		else
			$content .= $curl -> getResponseBody();

		file_put_contents( $file_path, $content );
	}

	private function generateLogFilePath( cURL $curl, $url )
	{
		$endpoint = strpos( $url, Config::$api_url ) !== false ?
			strtr( $url, [ Config::$api_url => '' ] ) :
			substr( $url, strpos( $url, '/', 10 ) + 1 );

		/** @noinspection RegExpRedundantEscape */
		$endpoint = preg_replace(
			[ '/\?.+$/', '/[^a-zA-Z0-9_ \.-]+/', '/^_+/', '/_+$/' ],
			[ '', '_', '', '' ],
			$endpoint
		);

		return rtrim( Config::$log_path, '/' ) . date('/Y/m/d/H:i:s:') .
			substr( microtime(), 2, 2 ) . '_' . $curl -> getRequestMethod() . '_' .
			$endpoint . '.log';
	}
}
