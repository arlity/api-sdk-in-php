<?php

namespace ArlitySDK\Lib;

class ResponseError
{
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data )
		{
			foreach( get_object_vars( $_obj_data ) as $key => $value )
			{
				$this -> $key = $value;
			}
		}
	}
}
