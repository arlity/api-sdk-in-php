<?php

namespace ArlitySDK\Lib\Entity;

use ArlitySDK\Lib\SDKException;

class ProductDetailsEntity extends AbstractEntity
{
	/**
	 * Kod EAN produktu.
	 * @var string
	 */
	public $ean;

	/**
	 * Cena produktu.
	 * @var string
	 */
	public $price;

	/**
	 * Zewnętrzny link do produktu.
	 * @var string
	 */
	public $external_url;

	/**
	 * Lista kombinacji produktu. Występuje tylko dla produktów o typie `combination`.
	 * @var ProductCombinationEntity[]
	 */
	public $combination = [];

	/**
	 * Opcje do wyświetlania obiektu w AR. Jeśli nie występują, to przyjmuje `null`.
	 * @var ARPresentationEntity|null
	 */
	public $ar_presentation = null;

	/**
	 * Pozycja do umieszczenia obiektu w przestrzeni. Jeśli nie występują, to przyjmuje `null`.
	 * @var ObjectPlacementEntity|null
	 */
	public $object_placement = null;

	/**
	 * Wymiary obiektu. Jeśli nie występują **wszystkie**, to przyjmuje `null`.
	 * @var ObjectSizeEntity|null
	 */
	public $object_size = null;

	/**
	 * @param mixed $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data )
		{
			if( property_exists( $_obj_data, 'combination' ) && !empty( $_obj_data -> combination ))
			{
				foreach( $_obj_data -> combination as $combination )
					$this -> combination[] = new ProductCombinationEntity( $combination );

				unset( $_obj_data -> combination );
			}
			if( property_exists( $_obj_data, 'ar_presentation' ) && $_obj_data -> ar_presentation )
			{
				$this -> ar_presentation = new ARPresentationEntity( $_obj_data -> ar_presentation );
				unset( $_obj_data -> ar_presentation );
			}
			if( property_exists( $_obj_data, 'object_placement' ) && $_obj_data -> object_placement )
			{
				$this -> object_placement = new ObjectPlacementEntity( $_obj_data -> object_placement );
				unset( $_obj_data -> object_placement );
			}
			if( property_exists( $_obj_data, 'object_size' ) && $_obj_data -> object_size )
			{
				$this -> object_size = new ObjectSizeEntity( $_obj_data -> object_size );
				unset( $_obj_data -> object_size );
			}
		}

		parent::__construct( $_obj_data );
	}
}