<?php

namespace ArlitySDK\Lib\Entity;

class ARPresentationEntity extends AbstractEntity
{
	/**
	 * Umieszczanie obiektu względem podanej wartości.
	 * @see PLACEMENT_ON_FLOOR
	 * @see PLACEMENT_HANGING
	 * @see PLACEMENT_CEILING
	 * @var string
	 */
	public $placement = null;

	const PLACEMENT_ON_FLOOR = 'standard';
	const PLACEMENT_HANGING = 'hanging';
	const PLACEMENT_CEILING = 'ceiling';
}