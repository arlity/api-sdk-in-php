<?php

namespace ArlitySDK\Lib\Entity;

/**
 * Parametry wejściowe dla tworzenia i edycji kategorii
 */
class CategoryEntity extends CategoryBasicDataEntity
{
	/**
	 * @var int
	 */
	public $created = null;

	/**
	 * @var int
	 */
	public $modified = null;

	/**
	 * @var bool
	 */
	public $removed = false;

	/**
	 * @var int
	 */
	public $parent_id = null;

	/**
	 * @var string|null Typ kategorii. Dla kategorii głównych dla firmy przyjmuje `company` lub `client`, w pozostałych przypadkach, będzie null.
	 * @readonly
	 */
	public $type = null;

	/**
	 * @var int
	 */
	public $order = null;

	/**
	 * @var string
	 */
	public $thumbnail_url = null;

	const TYPE_COMPANY = 'company';
	const TYPE_B2B = 'b2b';
	const TYPE_DEFAULT = null;
}