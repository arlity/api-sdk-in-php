<?php

namespace ArlitySDK\Lib\Entity;

use ArlitySDK\Lib\SDKException;

class ProductAttributeValueEntity extends AbstractEntity
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $uuid;

	/**
	 * @var string
	 */
	public $value;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var bool
	 */
	public $personalization = false;

	/**
	 * @var ThumbnailEntity|null
	 */
	public $thumbnail = null;

	/**
	 * @param mixed $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data )
		{
			if( property_exists( $_obj_data, 'thumbnail' ) && $_obj_data -> thumbnail )
			{
				$this -> thumbnail = new ThumbnailEntity( $_obj_data -> thumbnail );
				unset( $_obj_data -> thumbnail );
			}
		}

		parent::__construct( $_obj_data );
	}
}