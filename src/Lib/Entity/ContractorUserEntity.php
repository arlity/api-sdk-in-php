<?php

namespace ArlitySDK\Lib\Entity;

use ArlitySDK\Lib\SDKException;

/**
 * Parametry wejściowe dla tworzenia i edycji użytkownika
 */
class ContractorUserEntity extends AbstractEntity
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $uuid;

	/**
	 * @var string
	 */
	public $login;

	/**
	 * @var string
	 */
	public $password;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var int
	 */
	public $created;

	/**
	 * @var int
	 */
	public $modified;

	/**
	 * @var bool
	 */
	public $removed;

	/**
	 * @var ContractorBasicDataEntity
	 */
	public $contractor;

	/**
	 * @var GroupBasicDataEntity
	 */
	public $group;

	/**
	 * @var string
	 */
	public $email;

	/**
	 * @var string
	 */
	public $language;

	/**
	 * @var bool
	 */
	public $active;

	/**
	 * @param mixed $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data && property_exists( $_obj_data, 'contractor' ))
		{
			$this -> contractor = new ContractorBasicDataEntity( $_obj_data -> contractor );
			unset( $_obj_data -> contractor );
		}
		if( $_obj_data && property_exists( $_obj_data, 'group' ))
		{
			$this -> group = new GroupBasicDataEntity( $_obj_data -> group );
			unset( $_obj_data -> group );
		}
		parent::__construct( $_obj_data );
	}
}