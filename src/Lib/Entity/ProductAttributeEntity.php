<?php

namespace ArlitySDK\Lib\Entity;

use ArlitySDK\Lib\SDKException;

class ProductAttributeEntity extends AbstractEntity
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $code;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var string
	 */
	public $material_name = '';

	/**
	 * @var ProductAttributeValueEntity[]
	 */
	public $value = [];

	/**
	 * @param mixed $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data )
		{
			if( property_exists( $_obj_data, 'value' ) && !empty( $_obj_data -> value ))
			{
				foreach( $_obj_data -> value as $value )
					$this -> value[] = new ProductAttributeValueEntity( $value );

				unset( $_obj_data -> value );
			}
		}

		parent::__construct( $_obj_data );
	}
}