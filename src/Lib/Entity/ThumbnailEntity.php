<?php


namespace ArlitySDK\Lib\Entity;


class ThumbnailEntity extends AbstractEntity
{
	/**
	 * @var string
	 */
	public $thumb_url;

	/**
	 * @var string
	 */
	public $medium_url;

	/**
	 * @var string
	 */
	public $large_url = null;
}