<?php

namespace ArlitySDK\Lib\Entity;

/**
 * Uproszczone dane kategorii
 */
class CategoryBasicDataEntity extends AbstractEntity
{
	/**
	 * @var int
	 */
	public $id = null;

	/**
	 * @var string
	 */
	public $uuid = null;

	/**
	 * @var string
	 */
	public $name = null;
}