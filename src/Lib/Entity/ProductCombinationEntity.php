<?php

namespace ArlitySDK\Lib\Entity;

use ArlitySDK\Lib\SDKException;

class ProductCombinationEntity extends AbstractEntity
{
	/**
	 * Tablica obiektów dla atrybutów, z których powstał wariant.
	 * @var ProductCombinationAttributeEntity[]
	 */
	public $attribute = [];

	/**
	 * Flaga czy kombinacja jest domyślną.
	 * @var bool
	 */
	public $is_default = false;

	/**
	 * Obiekt produktu powstałego z tej kombinacji. Dane tutaj będą zawsze odpowiadać, temu jakbyśmy podali `parts` = `basic`.
	 * @var ProductEntity
	 */
	public $product;

	/**
	 * @param mixed $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data )
		{
			if( property_exists( $_obj_data, 'attribute' ) && !empty( $_obj_data -> attribute ))
			{
				foreach( $_obj_data -> attribute as $attribute_id => $value_id )
				{
					$entity = new ProductCombinationAttributeEntity();
					$entity -> attribute_id = $attribute_id;
					$entity -> value_id = $value_id;
					$this -> attribute[] = $entity;
				}

				unset( $_obj_data -> attribute );
			}
			if( property_exists( $_obj_data, 'product' ) && $_obj_data -> product )
			{
				$this -> product = new ProductEntity( $_obj_data -> product );
				unset( $_obj_data -> product );
			}
		}

		parent::__construct( $_obj_data );
	}
}