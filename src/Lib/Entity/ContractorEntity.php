<?php

namespace ArlitySDK\Lib\Entity;

/**
 * Parametry wejściowe dla tworzenia i edycji kontrahenta
 */
class ContractorEntity extends ContractorBasicDataEntity
{
	/**
	 * @var int
	 */
	public $created;

	/**
	 * @var int
	 */
	public $modified;

	/**
	 * @var bool
	 */
	public $removed;

	/**
	 * @var int
	 */
	public $parent_id;

	/**
	 * @var int
	 */
	public $default_group;

	/**
	 * @var string
	 */
	public $domain;
}