<?php

namespace ArlitySDK\Lib\Entity;

class ObjectSizeEntity extends AbstractEntity
{
	/**
	 * Szerokość obiektu, wyrażona w metrach.
	 * @var float
	 */
	public $width = null;

	/**
	 * Wysokość obiektu, wyrażona w metrach.
	 * @var float
	 */
	public $height = null;

	/**
	 * Głębokość obiektu, wyrażona w metrach.
	 * @var float
	 */
	public $depth = null;
}