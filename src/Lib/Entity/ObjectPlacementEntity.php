<?php

namespace ArlitySDK\Lib\Entity;

class ObjectPlacementEntity extends AbstractEntity
{
	/**
	 * Odległość umieszczenia w pionowej osi od podłogi, wyrażona w metrach.
	 * @var float|null
	 */
	public $y = null;
}