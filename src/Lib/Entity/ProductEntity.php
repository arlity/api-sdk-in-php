<?php

namespace ArlitySDK\Lib\Entity;


use ArlitySDK\Lib\SDKException;

class ProductEntity extends AbstractEntity
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 * @part basic
	 */
	public $uuid;

	/**
	 * Kod produktu.
	 * @var string
	 * @part basic
	 */
	public $code;

	/**
	 * Nazwa produktu w żądanym języku.
	 * @var string
	 * @part basic
	 */
	public $name;

	/**
	 * Flaga czy obiekt jest aktywny.
	 * @var bool
	 * @part status
	 */
	public $active;

	/**
	 * Flaga czy obiekt jest ukryty - nie powinien się wyświetlać w katalogu produktów.
	 * @var bool
	 * @part status
	 */
	public $hidden;

	/**
	 * Timestamp utworzenia.
	 * @var int
	 * @part status
	 */
	public $created;

	/**
	 * Timestamp ostatniej modyfikacji.
	 * @var int
	 * @part status
	 */
	public $modified;

	/**
	 * Flaga czy obiekt jest usunięty (usunięte obiekty są zwracane jedynie przy pobieraniu kolekcji z parametrem `since`, czyli w opcji synchronizacji).
	 * @var bool
	 * @part status
	 */
	public $removed;

	/**
	 * Opis w żądanym języku.
	 * @var string
	 * @part details
	 */
	public $description;

	/**
	 * Typ produktu.
	 * @var string
	 * @part details
	 */
	public $type;

	/**
	 * Obiekt produktu rodzica.
	 * Obecnie używane jedynie w przypadku produktów powstałych z kombinacji, rodzicem będzie produkt, z którego generowano te kombinacje.
	 * Dane tutaj będą zawsze odpowiadać, temu jakbyśmy podali `parts` = `basic`.
	 * @var ProductEntity
	 * @part details
	 */
	public $parent = null;

	/**
	 * Lista kategorii, w których jest produkt.
	 * @var CategoryBasicDataEntity[]
	 * @part presentation
	 */
	public $categories = [];

	/**
	 * Obiekt ze szczegółowymi danymi.
	 * @var ProductDetailsEntity
	 * @part details
	 */
	public $details = null;

	/**
	 * Lista atrybutów przypisanych do tego produktu.
	 * @var ProductAttributeEntity[]
	 * @part configuration
	 */
	public $attribute = [];

	/**
	 * Obiekt z miniaturkami.
	 * @var ThumbnailEntity
	 * @part thumbnail
	 */
	public $thumbnail = null;

	/**
	 * Link do pobrania zipa z plikami 3D.
	 * @var string
	 * @part configuration
	 */
	public $download_ar_zip = '';

	/**
	 * Link do karty produktu.
	 * @var string
	 * @part presentation
	 */
	public $preview_card_url = '';

	/**
	 * Link do pobrania pliku 3D (gltf lub obj).
	 * @var string
	 * @part configuration
	 */
	public $download_3d_model = '';

	const PART_BASIC = 'basic';
	const PART_STATUS = 'status';
	const PART_DETAILS = 'details';
	const PART_PRESENTATION = 'presentation';
	const PART_THUMBNAIL = 'thumbnail';
	const PART_CONFIGURATION = 'configuration';

	const TYPE_SIMPLE = 'simple';
	const TYPE_PACKAGE = 'package';
	const TYPE_MULTI_VARIANTS = 'combinations';
	const TYPE_VARIANT = 'variant';

	/**
	 * @param mixed $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( $_obj_data )
		{
			if( property_exists( $_obj_data, 'categories' ) && !empty( $_obj_data -> categories ))
			{
				foreach( $_obj_data -> categories as $category )
					$this -> categories[] = new CategoryBasicDataEntity( $category );

				unset( $_obj_data -> categories );
			}
			if( property_exists( $_obj_data, 'parent' ) && $_obj_data -> parent )
			{
				$this -> parent = new ProductEntity( $_obj_data -> parent );
				unset( $_obj_data -> parent );
			}
			if( property_exists( $_obj_data, 'details' ) && $_obj_data -> details )
			{
				$this -> details = new ProductDetailsEntity( $_obj_data -> details );
				unset( $_obj_data -> details );
			}
			if( property_exists( $_obj_data, 'attribute' ) && !empty( $_obj_data -> attribute ))
			{
				foreach( $_obj_data -> attribute as $attribute )
					$this -> attribute[] = new ProductAttributeEntity( $attribute );

				unset( $_obj_data -> attribute );
			}
			if( property_exists( $_obj_data, 'thumbnail' ) && $_obj_data -> thumbnail )
			{
				$this -> thumbnail = new ThumbnailEntity( $_obj_data -> thumbnail );
				unset( $_obj_data -> thumbnail );
			}
		}

		parent::__construct( $_obj_data );
	}
}