<?php

namespace ArlitySDK\Lib\Entity;

class ProductCombinationAttributeEntity extends AbstractEntity
{
	/**
	 * ID atrybutu.
	 * @var int
	 */
	public $attribute_id = null;

	/**
	 * ID wartości atrybutu.
	 * @var int
	 */
	public $value_id = null;
}