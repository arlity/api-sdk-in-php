<?php

namespace ArlitySDK\Lib\Entity;

/**
 * Uproszczone dane kontrahenta
 */
class ContractorBasicDataEntity extends AbstractEntity
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $uuid;

	/**
	 * @var string
	 */
	public $code;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string b2b|b2c
	 */
	public $type;

	const TYPE_B2B = 'b2b';
	const TYPE_B2C = 'b2c';
}