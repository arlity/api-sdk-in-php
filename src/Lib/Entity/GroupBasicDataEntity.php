<?php

namespace ArlitySDK\Lib\Entity;

/**
 * Uproszczone dane grup
 */
class GroupBasicDataEntity extends AbstractEntity
{
	/**
	 * @var string
	 */
	public $id = null;

	/**
	 * @var string
	 */
	public $code = null;

	/**
	 * @var string
	 */
	public $name = null;

	/**
	 * @var string
	 */
	public $type = null;

	const TYPE_ADMIN = 'admin';
	const TYPE_USER = 'user';
	const TYPE_B2B = 'b2b';
	const TYPE_API = 'api';
}