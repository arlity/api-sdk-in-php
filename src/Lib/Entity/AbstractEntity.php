<?php

namespace ArlitySDK\Lib\Entity;

use ArlitySDK\Lib\SDKException;

abstract class AbstractEntity
{
	/**
	 * Konstruktor pozwalający utworzyć obiekt z danych zwróconych z API
	 * @param array|\stdClass $_obj_data
	 * @throws SDKException
	 */
	public function __construct( $_obj_data = null )
	{
		if( is_null( $_obj_data ))
			$construct_data = [];
		else if( is_array( $_obj_data ))
			$construct_data = $_obj_data;
		else if( is_object( $_obj_data ))
			$construct_data = get_object_vars( $_obj_data );
		else
			throw new SDKException( 'Unknown type of param. Given: ' . gettype( $_obj_data ));

		foreach( $construct_data as $key => $value )
		{
			if( property_exists( $this, $key ))
				$this -> $key = $value;
		}
	}

	/**
	 * Magiczna metoda do ustawiania wartości właściwości
	 * @param string $key
	 * @param mixed $value
	 */
	public function __set( $key, $value )
	{
		if( method_exists( $this, '__set_'.$key ))
			$this -> {'__set_'.$key}( $value );
		else if( property_exists( $this, $key ))
			$this -> $key = $value;
	}

	/**
	 * Magiczna metoda do pobierania wartości właściwości
	 * @param string $key
	 * @return mixed
	 */
	public function __get( $key )
	{
		if( method_exists( $this, '__get_'.$key ))
			return $this -> {'__get_'.$key}();
		else if( property_exists( $this, $key ))
			return $this -> $key;
		else
			return null;
	}

	/**
	 * Magiczna metoda do sprawdzania istnienia właściwości
	 * @param string $key
	 * @return bool
	 */
	public function __isset( $key )
	{
		return property_exists( $this, $key );
	}
}