<?php

namespace ArlitySDK;

spl_autoload_register(
	function( $_class_name )
	{
		if( substr( $_class_name, 0, strlen( __NAMESPACE__.'\\' ) ) === __NAMESPACE__.'\\' )
		{
			/** @noinspection PhpIncludeInspection */
			include_once strtr( $_class_name, [ __NAMESPACE__.'\\' => '', '\\' => '/' ] ).'.php';
		}
	}
);
