<?php

namespace ArlitySDK;

use ArlitySDK\Lib\Resource\CategoryResource;
use ArlitySDK\Lib\Resource\ContractorResource;
use ArlitySDK\Lib\Resource\ContractorUserResource;
use ArlitySDK\Lib\Resource\ProductResource;
use ArlitySDK\Lib\Resource\AuthResource;

class SDK
{
	/**
	 * Obiekt klasy do autoryzacji
	 * @var AuthResource
	 */
	public $auth;

	/**
	 * Obiekt do obsługi api związanego z kontrahentami
	 * @var ContractorResource
	 */
	public $contractor;

	/**
	 * Obiekt do obsługi api związanego z użytkownikami kontrahentów
	 * @var ContractorUserResource
	 */
	public $contractor_user;

	/**
	 * Obiekt do obsługi produktów.
	 * @var ProductResource
	 */
	public $product;

	/**
	 * Obiekt do obsługi kategorii produktów, które są powiązane również z kontrahentami.
	 * @var CategoryResource
	 */
	public $category;

	/**
	 * SDK constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		$this -> auth = new AuthResource();
		$this -> contractor = new ContractorResource();
		$this -> contractor_user = new ContractorUserResource();
		$this -> product = new ProductResource();
		$this -> category = new CategoryResource();
	}
}
