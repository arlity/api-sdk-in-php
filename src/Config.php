<?php

namespace ArlitySDK;

class Config
{
	/**
	 * Klucz do api
	 * @var string
	 */
	static public $api_key = '';

	/**
	 * Sekretny kod, do wygenerowania tokenu
	 * @var string
	 */
	static public $api_secret = '';

	/**
	 * Adres do api
	 * @var string
	 */
	static public $api_url = 'http://v2.cloud.arlity.com/v4/api';

	/**
	 * Czy moduł ma logować zapytania do katalogu
	 * @see self::$log_path (trzeba nadać prawa zapisu)
	 * @var bool
	 */
	static public $log_curl_queries = true;

	/**
	 * Ścieżka do zapisu logów. Użytkownik uruchamiający SDK musi mieć uprawnienia zapisu.
	 * @var string
	 */
	static public $log_path = __DIR__.'/../logs';

	/**
	 * Katalog, w jakim mają być zapisywane domyślnie pliki przy pobieraniu
	 * @var string
	 */
	static public $download_dir = '/tmp';

	/**
	 * Nazwa/ścieżka do pliku, w którym ma być zapisywany token. Najlepiej, żeby nie był dostępny przez serwer www, można np. użyć katalogu `/tmp`
	 * Jeśli ustawimy false, token nie będzie zapisywany, za to będzie za każdym zapytaniem generowany na nowo, co spowolni działanie integracji
	 * @var string|false
	 */
	static public $token_file = '/tmp/Arlity_token';

	/**
	 * Jeśli ustawione na true -> za każdym wywołaniem testujemy token przez API w momencie autoryzacji. Wiąże się to z dodatkowym zapytaniem, ale jeśli używamy jednego konta na kilku systemach (a nie powinniśmy!) może to ograniczyć ilość błędów, które powinny się pojawić tylko, gdy inny system napisze token w momencie wykonywania jeszcze czynności przez stary.
	 * @var bool
	 */
	static public $always_test_token = false;
}
